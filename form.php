<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Form PHP</title>
	<style>
	   form {
	       width: 600px;
	       height: 700px;
	       background: #13B5EC;
	       border-radius: 8px;
	       margin: 0 auto;
	       padding: 30px;
	       box-shadow: 0px 0px 14px 0px rgba(46, 53, 55, 0.77);
	   }
	   p {
	       line-height: 0.5;
	   }
	   label {
	       margin: 3px;
	   }
	   input {
	       margin: 8px 0;
	   }
	   input[type="text"], input[type="email"] {
	       width: 100%;
	       height: 30px;
	       border-radius: 5px;
	       border: 2px solid grey;
	       outliine: none;
	       padding: 7px;
	   }
	   input[type="checkbox"] {
	       margin-right: 7px;
	   }
	   textarea {
	       width: 300px;
	       height: 150px;
	       padding: 7px;
	   }
	   input[type="submit"] {
	       padding: 7px 20px;
	       border-radius: 5px;
	       box-shadow: 0px 0px 5px 0px rgba(46, 53, 55, 0.5);
	   }
	   input[type="submit"]:hover {
	       cursor: pointer;
	   }
	</style>
</head>

<form action="" method="POST">
  <label>Your name</label>
  <input name="fio" type="text">
  <br>
  
  <label>Your email</label>
  <input name="email" type="email">
  <br>
  
  <p>Your year of birth</p>
  <select name="year">
  <?php for($i = 1900; $i < 2020; $i++) {?>
  	<option value="<?php print $i; ?>"><?= $i; ?></option>
  	<?php }?>
  </select>
  <br>
  
  <p>Your sex</p>
  <label class="radio">
		<input type="radio" name="sex" value="0" checked>
		Male
  </label>
  <label class="radio">
		<input type="radio" name="sex" value="1">
		Female
  </label>
  <br>
  
  <p>Number of limbs</p>
  <label class="radio">
		<input type="radio" name="limbs" value="2" checked>
		2
  </label>
  <label class="radio">
		<input type="radio" name="limbs" value="4">
		4
  </label>
  <label class="radio">
		<input type="radio" name="limbs" value="6">
		6
  </label>
  <label class="radio">
		<input type="radio" name="limbs" value="8">
		8
  </label>
  <label class="radio">
		<input type="radio" name="limbs" value="10">
		10
  </label>
  <br>
  <br>
	
  <select name="abilities[]" multiple>
  	<option value="immort">Immortable</option>
  	<option value="wall">Passing through walls</option>
  	<option value="levit">Levitation</option>
  	<option value="invis">Invisible</option>
  </select>
  <br>
  
  <p>Your biography</p>
  <textarea name="text" placeholder="Your biography" rows=10 cols=30></textarea>
  <br>
  
  <input type="checkbox" name="accept" value="1">Accept
  <br>
  <input type="submit" value="Submit">
</form>